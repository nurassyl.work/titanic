import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    # Extract the prefix from the 'name' column
    df['prefix'] = df['Name'].str.extract(r'(Mr\.|Mrs\.|Miss\.)')
    median_age = df.groupby('prefix')['Age'].median()
    missing_age_count = df.groupby('prefix')['Age'].apply(lambda x: x.isnull().sum())

    a = median_age.get('Mr.', 0)
    b = median_age.get('Mrs.', 0)
    c = median_age.get('Miss.', 0)
    x = missing_age_count.get('Mr.', 0)
    y = missing_age_count.get('Mrs.', 0)
    z = missing_age_count.get('Miss.', 0)
    result = [
    ('Mr.', x, a),
    ('Mrs.', y, b),
    ('Miss.', z, c)]
    return result
